import Vue from "vue";
import router from "./app.router.js";
import store from "./app.store.js";
import loader from "vue-ui-preloader";

Vue.use(loader);

Vue.config.productionTip = false;

// eslint-disable-next-line no-unused-vars
const app_vue = new Vue({
  el: "#app",

  components: {
    loader: loader,
  },

  router: router,

  store: store,

  data: {},

  computed: {},

  mounted: function () {
    this.$store.commit("setHomepageMsg", {
      msg: "Homepage",
    });

    this.$store.commit("setUsersMsg", {
      msg: "Users",
    });

    this.$store.commit("setPostsMsg", {
      msg: "Posts",
    });
  },

  methods: {},
});
