import Vue from "vue";
import VueRouter from "vue-router";
import Homepage from "../components/Homepage.vue";
import Users from "../components/Users.vue";
import Posts from "../components/Posts.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: Homepage,
    //redirect : '/homepage',
  },
  {
    path: "/users",
    component: Users,
  },
  {
    path: "/posts",
    component: Posts,
  },
];

const router = new VueRouter({
  routes: routes,
});

/*
 * Exports
 */
export default router;
